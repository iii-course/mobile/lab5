package com.example.lab5.models;

import android.graphics.Bitmap;

public class ImagesSet {
    public Bitmap originalImage;
    public Bitmap convertedImage;

    public ImagesSet(Bitmap originalImage, Bitmap convertedImage) {
        this.originalImage = originalImage;
        this.convertedImage = convertedImage;
    }
}
