package com.example.lab5.utils.helpers;

import android.graphics.Color;

public class PaintHelpers {
    public static int getRandomColor() {
        return Color.rgb((int) Math.round(Math.random() * 255),
                (int) Math.round(Math.random() * 255),
                (int) Math.round(Math.random() * 255));
    }

    public static float getRandomNumber(float min, float max) {
        return (float) Math.random() * (max + 1 - min) + min;
    }
}
