package com.example.lab5.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;

import com.example.lab5.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class Animation5Fragment extends Fragment {
    private FloatingActionButton upButton, downButton;
    private Animation5View view;

    private int speed = 5;
    private final int step = 1;

    private int maxSpeed = 100;
    private int minSpeed = -100;

    public Animation5Fragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_animation5, container, false);

        upButton = rootView.findViewById(R.id.upButton);
        downButton = rootView.findViewById(R.id.downButton);
        view = rootView.findViewById(R.id.animation5View);

        upButton.setOnClickListener(this::increaseSpeed);
        downButton.setOnClickListener(this::decreaseSpeed);

        view.setSpeed(speed);

        return rootView;
    }

    private void decreaseSpeed(View view) {
        if (speed - step < maxSpeed && speed - step > minSpeed) {
            speed -= step;
        }
        this.view.setSpeed(speed);
    }

    private void increaseSpeed(View view) {
        if (speed + step < maxSpeed && speed + step > minSpeed) {
            speed += step;
        }
        this.view.setSpeed(speed);
    }
}
