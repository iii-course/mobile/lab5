package com.example.lab5.ui;

import static com.example.lab5.utils.helpers.PaintHelpers.getRandomNumber;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;

public class Animation5View extends View {
    private float squareCentreX = 0, squareCentreY = 0;
    private int edgeLength = 300;

    private int speed = 1;
    private int currentAngle = 0;

    private float paintWidth = 10;

    private float radius = 0;
    private float circleCentreX = 0, circleCentreY = 0;

    public Animation5View(Context context) {
        super(context);
    }

    public Animation5View(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public Animation5View(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public Animation5View(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        drawCircle(canvas);

        currentAngle += speed;

        recalculateSquareCentre();
        drawSquare(canvas);

        invalidate();
    }

    private void setUpCircleCharacteristics(Canvas canvas) {
        circleCentreX = canvas.getWidth() / 2;
        circleCentreY = canvas.getHeight() / 2;
        radius = canvas.getWidth() / 3;
    }

    private void recalculateSquareCentre() {
        squareCentreX = circleCentreX + (float) (radius * Math.cos(getRadian(currentAngle)));
        squareCentreY = circleCentreY + (float) (radius * Math.sin(getRadian(currentAngle)));
    }

    private void drawSquare(Canvas canvas) {
        Paint paint = getPaint(Color.GREEN);
        float topX = squareCentreX - edgeLength / 2;
        float topY = squareCentreY - edgeLength / 2;
        canvas.drawPoint(squareCentreX, squareCentreY, paint);
        canvas.drawRect(topX, topY, topX + edgeLength, topY + edgeLength, paint);
    }

    private void drawCircle(Canvas canvas) {
        setUpCircleCharacteristics(canvas);
        Paint paint = getPaint(Color.MAGENTA);
        canvas.drawPoint(circleCentreX, circleCentreY, paint);
        canvas.drawCircle(circleCentreX, circleCentreY, radius, paint);
    }

    private Paint getPaint(int color) {
        Paint paint = new Paint();
        paint.setColor(color);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(paintWidth);
        return paint;
    }

    private float getRadian(float angle) {
        return (float) (angle * Math.PI / 180);
    }
}
