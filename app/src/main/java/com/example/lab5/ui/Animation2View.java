package com.example.lab5.ui;

import static com.example.lab5.utils.helpers.PaintHelpers.getRandomColor;
import static com.example.lab5.utils.helpers.PaintHelpers.getRandomNumber;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.View;

public class Animation2View extends View {
    private int x = 0;
    private int y = 0;

    private int width = 300;
    private int height = 500;

    private int xSpeed = 8;
    private int ySpeed = 8;

    private float paintWidth = 1;
    private int color = Color.RED;

    public Animation2View(Context context) {
        super(context);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        x = x + xSpeed;
        y = y + ySpeed;

        boolean changeWidthAndColor = false;

        boolean xShouldChangeDirectionToOpposite = x > canvas.getWidth() - width || x < 0;
        if (xShouldChangeDirectionToOpposite) {
            xSpeed = -xSpeed;
            changeWidthAndColor = true;
        }

        boolean yShouldChangeDirectionToOpposite = y > canvas.getHeight() - height || y < 0;
        if (yShouldChangeDirectionToOpposite) {
            ySpeed = -ySpeed;
            changeWidthAndColor = true;
        }

        Paint paint = getPaint(changeWidthAndColor);

        canvas.drawRect(x, y, x + width, y + height, paint);

        invalidate();
    }

    private Paint getPaint(boolean changeWidthAndColor) {
        Paint paint = new Paint();
        if (changeWidthAndColor) {
            paintWidth = getRandomNumber(1, 50);
            color = getRandomColor();
        }
        paint.setColor(color);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(paintWidth);
        return paint;
    }
}