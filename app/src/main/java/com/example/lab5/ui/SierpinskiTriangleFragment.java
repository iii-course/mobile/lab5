package com.example.lab5.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import com.example.lab5.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class SierpinskiTriangleFragment extends Fragment {
    public SierpinskiTriangleFragment() {}

    private FloatingActionButton pauseButton, resumeButton;
    private SierpinskiTriangleView view;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_sierpinski_triangle, container, false);

        pauseButton = rootView.findViewById(R.id.pauseButton);
        resumeButton = rootView.findViewById(R.id.resumeButton);
        view = rootView.findViewById(R.id.triangleView);

        pauseButton.setOnClickListener(this::pause);
        resumeButton.setOnClickListener(this::resume);

        return rootView;
    }

    private void resume(View view) {
        this.view.resume();
    }

    private void pause(View view) {
        this.view.pause();
    }

}
