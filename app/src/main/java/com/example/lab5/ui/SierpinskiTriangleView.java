package com.example.lab5.ui;

import static android.graphics.Bitmap.Config.ARGB_8888;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.CountDownTimer;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import androidx.annotation.Nullable;

import java.util.Arrays;

public class SierpinskiTriangleView extends View {
    private boolean paused = false;

    private int color = Color.BLACK;
    private int paintWidth = 5;
    private Paint paint = new Paint();

    int[] prevRow, curRow;

    public SierpinskiTriangleView(Context context) {
        super(context);
        setUpPaint();
    }

    public SierpinskiTriangleView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        setUpPaint();
    }

    public SierpinskiTriangleView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setUpPaint();
    }

    public SierpinskiTriangleView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        setUpPaint();
    }

    public void pause() {
        paused = true;
    }

    public void resume() {
        if (paused) {
            paused = false;
            this.invalidate();
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        initializeRowsIfNeeded(canvas);

        for (int i = 0; i < canvas.getHeight(); i += 1) {
            fillTheRow(i, canvas);
        }

        if (!paused) {
            invalidate();
        }
    }

    private void initializeRowsIfNeeded(Canvas canvas) {
        if (prevRow == null) {
            prevRow = new int[canvas.getWidth()];
        }

        if (curRow == null) {
            curRow = new int[canvas.getWidth()];
        }
    }

    private void fillTheRow(int row, Canvas canvas) {
        if (row == 0) {
            curRow[1] = 1;
            canvas.drawPoint(1, 0, paint);
        } else {
            prevRow = Arrays.copyOf(curRow, curRow.length);
            for (int i = 1; i < curRow.length; i += 1) {
                curRow[i] = prevRow[i - 1] ^ prevRow[i];
                if (curRow[i] == 1) {
                    canvas.drawPoint(i, row, paint);
                }
            }
        }
    }

    private void setUpPaint() {
        paint.setColor(color);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(paintWidth);
    }
}
