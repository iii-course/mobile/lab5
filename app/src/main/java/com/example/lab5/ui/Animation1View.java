package com.example.lab5.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.View;

import com.example.lab5.R;

public class Animation1View extends View {
    private int x = 0;
    private int y = 300;
    private int width = 300;
    private int height = 500;
    private int xSpeed = 8;

    public Animation1View(Context context) {
        super(context);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        Paint paint = getPaint();

        x = x + xSpeed;

        boolean shouldChangeDirectionToOpposite = x > canvas.getWidth() - width || x < 0;
        if (shouldChangeDirectionToOpposite) {
            xSpeed = -xSpeed;
        }
        canvas.drawRect(x, y, x + width, y + height, paint);

        invalidate();
    }

    private Paint getPaint() {
        Paint paint = new Paint();
        paint.setColor(Color.RED);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(20);
        return paint;
    }
}