package com.example.lab5.ui;

import static com.example.lab5.utils.helpers.PaintHelpers.getRandomColor;
import static com.example.lab5.utils.helpers.PaintHelpers.getRandomNumber;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.View;

public class Animation3View extends View {
    private int[] x = {0, 10};
    private int[] y = {0, 10};

    private int[] width = {500, 300};
    private int[] height = {300, 500};

    private int[] xSpeed = {8, 10};
    private int[] ySpeed = {8, 10};

    private float paintWidth = 1;
    private int color = Color.RED;

    public Animation3View(Context context) {
        super(context);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        drawRect(canvas, 0);
        drawRect(canvas, 1);

        invalidate();
    }

    private void drawRect(Canvas canvas, int rectNumber) {
        if (rectNumber >= x.length) {
            return;
        }

        x[rectNumber] = x[rectNumber] + xSpeed[rectNumber];
        y[rectNumber] = y[rectNumber] + ySpeed[rectNumber];

        boolean changeWidthAndColor = false;

        boolean xShouldChangeDirectionToOpposite = x[rectNumber] > canvas.getWidth() - width[rectNumber] || x[rectNumber] < 0;
        if (xShouldChangeDirectionToOpposite) {
            xSpeed[rectNumber] = -xSpeed[rectNumber];
            changeWidthAndColor = true;
        }

        boolean yShouldChangeDirectionToOpposite = y[rectNumber] > canvas.getHeight() - height[rectNumber] || y[rectNumber] < 0;
        if (yShouldChangeDirectionToOpposite) {
            ySpeed[rectNumber] = -ySpeed[rectNumber];
            changeWidthAndColor = true;
        }

        Paint paint = getPaint(changeWidthAndColor);
        canvas.drawRect(
                x[rectNumber],
                y[rectNumber],
                x[rectNumber] + width[rectNumber],
                y[rectNumber] + height[rectNumber],
                paint
        );

        invalidate();
    }

    private Paint getPaint(boolean changeWidthAndColor) {
        Paint paint = new Paint();
        if (changeWidthAndColor) {
            paintWidth = getRandomNumber(1, 50);
            color = getRandomColor();
        }
        paint.setColor(color);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(paintWidth);
        return paint;
    }
}
