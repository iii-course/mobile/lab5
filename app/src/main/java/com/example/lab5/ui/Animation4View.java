package com.example.lab5.ui;

import static com.example.lab5.utils.helpers.PaintHelpers.getRandomColor;
import static com.example.lab5.utils.helpers.PaintHelpers.getRandomNumber;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.View;

public class Animation4View extends View {
    private int x = 0, y = 0;
    private int width = 300, height = 500;
    private int xSpeed = 5, ySpeed = 5;

    private int offset = 500;

    private float paintWidth = 1;
    private int color = Color.RED;

    private int radius = 50;
    private float cx = x + width / 2;
    private float cy = y + height / 2;

    public Animation4View(Context context) {
        super(context);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        boolean hasChangedDirection = false;

        boolean xShouldChangeDirectionToOpposite = x + xSpeed + offset > canvas.getWidth() - width || x + xSpeed < 0;
        if (xShouldChangeDirectionToOpposite) {
            xSpeed = -xSpeed;
            hasChangedDirection = true;
        }

        boolean yShouldChangeDirectionToOpposite = y + ySpeed + offset > canvas.getHeight() - height || y + ySpeed < 0;
        if (yShouldChangeDirectionToOpposite) {
            ySpeed = -ySpeed;
            hasChangedDirection = true;
        }

        x = x + xSpeed;
        y = y + ySpeed;

        if (hasChangedDirection) {
            changeCircleCentre();
        } else {
            updateCircleCentre();
        }

        Paint paint = getPaint(hasChangedDirection);

        canvas.drawRect(x, y, x + width, y + height, paint);
        canvas.drawRect(x + offset, y + offset, x + width + offset, y + height + offset, paint);

        canvas.drawCircle(cx, cy, radius, paint);
        canvas.drawCircle(cx + offset, cy + offset, radius, paint);

        invalidate();
    }

    private void updateCircleCentre() {
        cx = cx + xSpeed;
        cy = cy + ySpeed;
    }

    private void changeCircleCentre() {
        cx = x + width / 2 + getRandomNumber(-100, 100);
        cy = y + height / 2 + getRandomNumber(-100, 100);
    }

    private Paint getPaint(boolean changeWidthAndColor) {
        Paint paint = new Paint();
        if (changeWidthAndColor) {
            paintWidth = getRandomNumber(1, 50);
            color = getRandomColor();
        }
        paint.setColor(color);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(paintWidth);
        return paint;
    }
}
